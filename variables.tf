
/* region working in*/
variable "region" {
  type        = string
  default     = "us-east-2"
  description = "default region"
}

/*key file*/
variable "certkey" {
  type        = string
  default     = "skc-aws-private.key"
  description = "self signed key"
}

/*pem file*/
variable "certpem" {
  type        = string
  default     = "skc-aws-private.pem"
  description = "description"
}


variable "elb_security_policy" {
  default = "ELBSecurityPolicy-TLS-1-2-2017-01"
}

variable "enable_application_load_balancer" {
  description = "true to turn on skc_elb module"
  default     = true
}

variable "skc_enable" {
  description = "skc enable value"

}

variable "cert_to_use" {
  description = "this value will identify which certificate to apply 0 or 1"
}
