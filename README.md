<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-refresh-toc -->
**Table of Contents**

- [ELB Security Policy Changes](#elb-security-policy-changes)
    - [Problem Statement](#problem-statement)
- [Test to reporduce the problem](#test-to-reporduce-the-problem)
    - [self signed certificate](#self-signed-certificate)
    - [Steps to reporduce the problem](#steps-to-reporduce-the-problem)
        - [initial run](#initial-run)
        - [run one](#run-one)
        - [run two](#run-two)

<!-- markdown-toc end -->




# ELB Security Policy Changes

## Problem Statement

Every time `terraform apply` is run, the resource `aws_iam_server_certificate` gets executed and creates a new `ssl_certificate_id`. When a new `ssl_certificate_id` is introduced, the `Security Policy` changes. 


# Test to reporduce the problem

##  self signed certificate

- [ ] a key and a pem file are needed for testing 

```
openssl genrsa 2048 > skc-aws-private.key

openssl req -new -x509 -nodes -days 365000 \
   -key skc-aws-private.key \
   -out skc-aws-private.pem
```

## Steps to reporduce the problem

### initial run
- [ ] makefile is included with targets that will reproduce the issue
  - [ ] `make runinitial`
    - this target will do the initial run and create two aws_iam_server_certificate.certs, [0] and [1]. cert[0] is applied on the initial run. 
	- initial security policy
	  - `ELBSecurityPolicy-TLS-1-2-2017-01`
	  - initial run to setup environment.
	  
![](initial_security_policy.png)

### run one
  - [ ] `make runone`
    - this target will use cert[0] for its execution
	- runone security policy
	  - `ELBSecurityPolicy-TLS-1-2-2017-01`
	  - the security policy did not change because cert[0] was used. the cert did **not** change.
	  
![](runone_security_policy.png)

### run two
   - [ ] `make runtwo`
     - this target will use cert[1] for its execution
	 - runtwo security policy
	   - `ELBSecurityPolicy-2016-08`
	   - the security policy change because cert[1] was used. the cert **did** change.
	   
![](runtwo_security_policy.png)
