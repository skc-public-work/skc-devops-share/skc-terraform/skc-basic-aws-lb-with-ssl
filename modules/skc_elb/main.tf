

/*
Module: skc_elb
*/

/* create a certificate */
resource "aws_iam_server_certificate" "cert" {
  count  = var.enable[0]
  name_prefix      = "private-cert"
  certificate_body = file(var.certpem)
  private_key      = file(var.certkey)

  lifecycle {
    create_before_destroy = true
  }

}


resource "aws_elb" "skc_elb" {
  name            = "skc-test-elb"
  subnets              = [var.subnets[0]]
  security_groups    = [var.security_groups[0]]
  
  listener {
    instance_port     = 80
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }

   listener {
    instance_port     = 80
    instance_protocol = "http"
    lb_port           = 443
    lb_protocol       = "https"
    ssl_certificate_id = aws_iam_server_certificate.cert[var.cert_applied].arn
  }

  tags = {
    Name = "skc_elb"
    source = "terraform"
  }
}


resource "aws_load_balancer_policy" "skc_listener_policy-tls-1-2" {
  load_balancer_name = aws_elb.skc_elb.name
  policy_name = "skc-tls-1-2"
  policy_type_name = "SSLNegotiationPolicyType"

  policy_attribute {
    name = "Reference-Security-Policy"
    value = var.elb_security_policy
  }
}

resource "aws_load_balancer_listener_policy" "skc_listener_policies" {
  load_balancer_name = aws_elb.skc_elb.name
  load_balancer_port = 443

  policy_names = [
    aws_load_balancer_policy.skc_listener_policy-tls-1-2.policy_name
  ]
}

