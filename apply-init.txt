
Terraform used the selected providers to generate the following execution
plan. Resource actions are indicated with the following symbols:
  + create

Terraform will perform the following actions:

  # aws_internet_gateway.skc_internet_gw will be created
  + resource "aws_internet_gateway" "skc_internet_gw" {
      + arn      = (known after apply)
      + id       = (known after apply)
      + owner_id = (known after apply)
      + tags     = {
          + "Name"   = "skc_int_gw"
          + "source" = "terraform"
        }
      + tags_all = {
          + "Name"   = "skc_int_gw"
          + "source" = "terraform"
        }
      + vpc_id   = (known after apply)
    }

  # aws_security_group.skc_elb_sg will be created
  + resource "aws_security_group" "skc_elb_sg" {
      + arn                    = (known after apply)
      + description            = "Managed by Terraform"
      + egress                 = [
          + {
              + cidr_blocks      = [
                  + "0.0.0.0/0",
                ]
              + description      = ""
              + from_port        = 0
              + ipv6_cidr_blocks = []
              + prefix_list_ids  = []
              + protocol         = "-1"
              + security_groups  = []
              + self             = false
              + to_port          = 0
            },
        ]
      + id                     = (known after apply)
      + ingress                = [
          + {
              + cidr_blocks      = [
                  + "0.0.0.0/0",
                ]
              + description      = ""
              + from_port        = 443
              + ipv6_cidr_blocks = []
              + prefix_list_ids  = []
              + protocol         = "tcp"
              + security_groups  = []
              + self             = false
              + to_port          = 443
            },
          + {
              + cidr_blocks      = [
                  + "0.0.0.0/0",
                ]
              + description      = ""
              + from_port        = 80
              + ipv6_cidr_blocks = []
              + prefix_list_ids  = []
              + protocol         = "tcp"
              + security_groups  = []
              + self             = false
              + to_port          = 80
            },
        ]
      + name                   = "skc_elb_sg"
      + name_prefix            = (known after apply)
      + owner_id               = (known after apply)
      + revoke_rules_on_delete = false
      + tags                   = {
          + "Name"   = "skc_elb_sg"
          + "source" = "terraform"
        }
      + tags_all               = {
          + "Name"   = "skc_elb_sg"
          + "source" = "terraform"
        }
      + vpc_id                 = (known after apply)
    }

  # aws_subnet.skc_public will be created
  + resource "aws_subnet" "skc_public" {
      + arn                             = (known after apply)
      + assign_ipv6_address_on_creation = false
      + availability_zone               = "us-east-2a"
      + availability_zone_id            = (known after apply)
      + cidr_block                      = "10.0.0.0/16"
      + id                              = (known after apply)
      + ipv6_cidr_block_association_id  = (known after apply)
      + map_public_ip_on_launch         = true
      + owner_id                        = (known after apply)
      + tags                            = {
          + "Name"   = "Skc Public Subnet"
          + "source" = "terraform"
        }
      + tags_all                        = {
          + "Name"   = "Skc Public Subnet"
          + "source" = "terraform"
        }
      + vpc_id                          = (known after apply)
    }

  # aws_vpc.skc_vpc_main will be created
  + resource "aws_vpc" "skc_vpc_main" {
      + arn                              = (known after apply)
      + assign_generated_ipv6_cidr_block = false
      + cidr_block                       = "10.0.0.0/16"
      + default_network_acl_id           = (known after apply)
      + default_route_table_id           = (known after apply)
      + default_security_group_id        = (known after apply)
      + dhcp_options_id                  = (known after apply)
      + enable_classiclink               = (known after apply)
      + enable_classiclink_dns_support   = (known after apply)
      + enable_dns_hostnames             = true
      + enable_dns_support               = true
      + id                               = (known after apply)
      + instance_tenancy                 = "default"
      + ipv6_association_id              = (known after apply)
      + ipv6_cidr_block                  = (known after apply)
      + main_route_table_id              = (known after apply)
      + owner_id                         = (known after apply)
      + tags                             = {
          + "Name"   = "SKC Main VPC"
          + "source" = "terraform"
        }
      + tags_all                         = {
          + "Name"   = "SKC Main VPC"
          + "source" = "terraform"
        }
    }

  # module.skc_elb.aws_elb.skc_elb will be created
  + resource "aws_elb" "skc_elb" {
      + arn                         = (known after apply)
      + availability_zones          = (known after apply)
      + connection_draining         = false
      + connection_draining_timeout = 300
      + cross_zone_load_balancing   = true
      + dns_name                    = (known after apply)
      + id                          = (known after apply)
      + idle_timeout                = 60
      + instances                   = (known after apply)
      + internal                    = (known after apply)
      + name                        = "skc-test-elb"
      + security_groups             = (known after apply)
      + source_security_group       = (known after apply)
      + source_security_group_id    = (known after apply)
      + subnets                     = (known after apply)
      + tags                        = {
          + "Name"   = "skc_elb"
          + "source" = "terraform"
        }
      + tags_all                    = {
          + "Name"   = "skc_elb"
          + "source" = "terraform"
        }
      + zone_id                     = (known after apply)

      + health_check {
          + healthy_threshold   = (known after apply)
          + interval            = (known after apply)
          + target              = (known after apply)
          + timeout             = (known after apply)
          + unhealthy_threshold = (known after apply)
        }

      + listener {
          + instance_port      = 80
          + instance_protocol  = "http"
          + lb_port            = 443
          + lb_protocol        = "https"
          + ssl_certificate_id = (known after apply)
        }
      + listener {
          + instance_port     = 80
          + instance_protocol = "http"
          + lb_port           = 80
          + lb_protocol       = "http"
        }
    }

  # module.skc_elb.aws_iam_server_certificate.cert[0] will be created
  + resource "aws_iam_server_certificate" "cert" {
      + arn              = (known after apply)
      + certificate_body = <<-EOT
            -----BEGIN CERTIFICATE-----
            MIID7zCCAtegAwIBAgIUXYrQBymM8GWSStoNY4pR791Re7swDQYJKoZIhvcNAQEL
            BQAwgYUxCzAJBgNVBAYTAlVTMREwDwYDVQQIDAhOZXcgWW9yazEOMAwGA1UEBwwF
            SG9tZXIxFTATBgNVBAoMDHN0ZXAyaGVuLmNvbTEVMBMGA1UEAwwMc3RlcDJoZW4u
            Y29tMSUwIwYJKoZIhvcNAQkBFhZza2NvbmtsaW5AdGhpbmtlZGcuY29tMCAXDTIz
            MDExMzIwNDQ1MFoYDzMwMjIwNTE2MjA0NDUwWjCBhTELMAkGA1UEBhMCVVMxETAP
            BgNVBAgMCE5ldyBZb3JrMQ4wDAYDVQQHDAVIb21lcjEVMBMGA1UECgwMc3RlcDJo
            ZW4uY29tMRUwEwYDVQQDDAxzdGVwMmhlbi5jb20xJTAjBgkqhkiG9w0BCQEWFnNr
            Y29ua2xpbkB0aGlua2VkZy5jb20wggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEK
            AoIBAQCav410U2BibrouZiVg2wPtqpYklr1BU6x2WfEt1llyql0vFwj6kmh1Z/93
            nFKrQPtubDqeEtJhvhM6HcSVOLv02QHhyR+v2yNi/G9GPM+8kmnveJcADDxiTsTT
            5Bt7ktlOmxcmOcIJDqimOMD8k6Xx4Am/YcTMxqJfPhNOvwSW0D16vH5HUvGTJzAG
            0EvjfI6HzTkx1nsyjYBzlttscmbS+5EstZ1Fc6jRPuSPRnhFlR9Xt6cUPf4OKE/n
            ElWkTdcW0dSyqCNkFQntEglRVStK6fehDzNh/XvL++j4i28VbH/Uugit8h+QJhuL
            I/nx5k/sYT6GNPJjdVLJeypdjrPPAgMBAAGjUzBRMB0GA1UdDgQWBBThRgoju0yW
            288sutGVnhdQiAodJzAfBgNVHSMEGDAWgBThRgoju0yW288sutGVnhdQiAodJzAP
            BgNVHRMBAf8EBTADAQH/MA0GCSqGSIb3DQEBCwUAA4IBAQAo2hFsFwFH2n5ffn+D
            W1WL6TRQflYRjPrE0CCemBYFvLIJkSYThMdInCiUtLQzJ0jTkKweUYH0e+HL6sKQ
            /bjA6buTVPiT8iHT9u2ow6SpPooTWaaMXrjIYGNSqeIA+BpcL29slwZIvZgZg/76
            p8jGalLg6d6eeXiJtjimqni3sWj8Pse55hZlTm6KQgVD6M76PKckZHx7y9KGMIXt
            F4VjiGXL+I0rGgTrHfnbeIb7lUoKKZDoue6KZAIgjmgNJbb8Jgz/OrXrAPQqxByy
            Dqd0O3Eeg/QA0ivrhSlNIhY4yxeU3t2SdyjTZLWOFhNFbhaNlY7zKNomS82sAw2V
            VRHN
            -----END CERTIFICATE-----
        EOT
      + expiration       = (known after apply)
      + id               = (known after apply)
      + name             = (known after apply)
      + name_prefix      = "private-cert"
      + path             = "/"
      + private_key      = (sensitive value)
      + tags_all         = (known after apply)
      + upload_date      = (known after apply)
    }

  # module.skc_elb.aws_iam_server_certificate.cert[1] will be created
  + resource "aws_iam_server_certificate" "cert" {
      + arn              = (known after apply)
      + certificate_body = <<-EOT
            -----BEGIN CERTIFICATE-----
            MIID7zCCAtegAwIBAgIUXYrQBymM8GWSStoNY4pR791Re7swDQYJKoZIhvcNAQEL
            BQAwgYUxCzAJBgNVBAYTAlVTMREwDwYDVQQIDAhOZXcgWW9yazEOMAwGA1UEBwwF
            SG9tZXIxFTATBgNVBAoMDHN0ZXAyaGVuLmNvbTEVMBMGA1UEAwwMc3RlcDJoZW4u
            Y29tMSUwIwYJKoZIhvcNAQkBFhZza2NvbmtsaW5AdGhpbmtlZGcuY29tMCAXDTIz
            MDExMzIwNDQ1MFoYDzMwMjIwNTE2MjA0NDUwWjCBhTELMAkGA1UEBhMCVVMxETAP
            BgNVBAgMCE5ldyBZb3JrMQ4wDAYDVQQHDAVIb21lcjEVMBMGA1UECgwMc3RlcDJo
            ZW4uY29tMRUwEwYDVQQDDAxzdGVwMmhlbi5jb20xJTAjBgkqhkiG9w0BCQEWFnNr
            Y29ua2xpbkB0aGlua2VkZy5jb20wggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEK
            AoIBAQCav410U2BibrouZiVg2wPtqpYklr1BU6x2WfEt1llyql0vFwj6kmh1Z/93
            nFKrQPtubDqeEtJhvhM6HcSVOLv02QHhyR+v2yNi/G9GPM+8kmnveJcADDxiTsTT
            5Bt7ktlOmxcmOcIJDqimOMD8k6Xx4Am/YcTMxqJfPhNOvwSW0D16vH5HUvGTJzAG
            0EvjfI6HzTkx1nsyjYBzlttscmbS+5EstZ1Fc6jRPuSPRnhFlR9Xt6cUPf4OKE/n
            ElWkTdcW0dSyqCNkFQntEglRVStK6fehDzNh/XvL++j4i28VbH/Uugit8h+QJhuL
            I/nx5k/sYT6GNPJjdVLJeypdjrPPAgMBAAGjUzBRMB0GA1UdDgQWBBThRgoju0yW
            288sutGVnhdQiAodJzAfBgNVHSMEGDAWgBThRgoju0yW288sutGVnhdQiAodJzAP
            BgNVHRMBAf8EBTADAQH/MA0GCSqGSIb3DQEBCwUAA4IBAQAo2hFsFwFH2n5ffn+D
            W1WL6TRQflYRjPrE0CCemBYFvLIJkSYThMdInCiUtLQzJ0jTkKweUYH0e+HL6sKQ
            /bjA6buTVPiT8iHT9u2ow6SpPooTWaaMXrjIYGNSqeIA+BpcL29slwZIvZgZg/76
            p8jGalLg6d6eeXiJtjimqni3sWj8Pse55hZlTm6KQgVD6M76PKckZHx7y9KGMIXt
            F4VjiGXL+I0rGgTrHfnbeIb7lUoKKZDoue6KZAIgjmgNJbb8Jgz/OrXrAPQqxByy
            Dqd0O3Eeg/QA0ivrhSlNIhY4yxeU3t2SdyjTZLWOFhNFbhaNlY7zKNomS82sAw2V
            VRHN
            -----END CERTIFICATE-----
        EOT
      + expiration       = (known after apply)
      + id               = (known after apply)
      + name             = (known after apply)
      + name_prefix      = "private-cert"
      + path             = "/"
      + private_key      = (sensitive value)
      + tags_all         = (known after apply)
      + upload_date      = (known after apply)
    }

  # module.skc_elb.aws_load_balancer_listener_policy.skc_listener_policies will be created
  + resource "aws_load_balancer_listener_policy" "skc_listener_policies" {
      + id                 = (known after apply)
      + load_balancer_name = "skc-test-elb"
      + load_balancer_port = 443
      + policy_names       = [
          + "skc-tls-1-2",
        ]
    }

  # module.skc_elb.aws_load_balancer_policy.skc_listener_policy-tls-1-2 will be created
  + resource "aws_load_balancer_policy" "skc_listener_policy-tls-1-2" {
      + id                 = (known after apply)
      + load_balancer_name = "skc-test-elb"
      + policy_name        = "skc-tls-1-2"
      + policy_type_name   = "SSLNegotiationPolicyType"

      + policy_attribute {
          + name  = "Reference-Security-Policy"
          + value = "ELBSecurityPolicy-TLS-1-2-2017-01"
        }
    }

Plan: 9 to add, 0 to change, 0 to destroy.

Changes to Outputs:
  + main_skc_elb_sg    = (known after apply)
  + main_skc_subnet_id = (known after apply)
  + module_mod_id_arn  = (known after apply)
  + skc_vpc            = (known after apply)
  + var_skc_enable     = [
      + 2,
    ]
module.skc_elb.aws_iam_server_certificate.cert[1]: Creating...
module.skc_elb.aws_iam_server_certificate.cert[0]: Creating...
aws_vpc.skc_vpc_main: Creating...
module.skc_elb.aws_iam_server_certificate.cert[0]: Creation complete after 0s [id=ASCAUZI6P2674PPMV6OB4]
module.skc_elb.aws_iam_server_certificate.cert[1]: Creation complete after 0s [id=ASCAUZI6P267TZSVMPJPR]
aws_vpc.skc_vpc_main: Still creating... [10s elapsed]
aws_vpc.skc_vpc_main: Creation complete after 12s [id=vpc-02598547b9a8969b3]
aws_internet_gateway.skc_internet_gw: Creating...
aws_subnet.skc_public: Creating...
aws_security_group.skc_elb_sg: Creating...
aws_internet_gateway.skc_internet_gw: Creation complete after 1s [id=igw-06b9cdcea96f09f1f]
aws_security_group.skc_elb_sg: Creation complete after 2s [id=sg-09120575685e02e87]
aws_subnet.skc_public: Still creating... [10s elapsed]
aws_subnet.skc_public: Creation complete after 11s [id=subnet-070b74c67d6ac05a0]
module.skc_elb.aws_elb.skc_elb: Creating...
module.skc_elb.aws_elb.skc_elb: Creation complete after 2s [id=skc-test-elb]
module.skc_elb.aws_load_balancer_policy.skc_listener_policy-tls-1-2: Creating...
module.skc_elb.aws_load_balancer_policy.skc_listener_policy-tls-1-2: Creation complete after 0s [id=skc-test-elb:skc-tls-1-2]
module.skc_elb.aws_load_balancer_listener_policy.skc_listener_policies: Creating...
module.skc_elb.aws_load_balancer_listener_policy.skc_listener_policies: Creation complete after 0s [id=skc-test-elb:443]

Apply complete! Resources: 9 added, 0 changed, 0 destroyed.

Outputs:

main_skc_elb_sg = "sg-09120575685e02e87"
main_skc_subnet_id = "subnet-070b74c67d6ac05a0"
module_mod_id_arn = "arn:aws:iam::329165821887:server-certificate/private-cert20230124135043084400000002"
skc_vpc = "vpc-02598547b9a8969b3"
var_skc_enable = [
  2,
]
