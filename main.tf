

terraform {
  required_version = "= 1.1.9"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "3.64.0"
    }
  }
}

provider "aws" {
  region   = var.region
  insecure = true
}

# Main VPC
resource "aws_vpc" "skc_vpc_main" {
  cidr_block = "10.0.0.0/16"

  enable_dns_support   = true
  enable_dns_hostnames = true

  tags = {
    Name   = "SKC Main VPC"
    source = "terraform"
  }
}

/*
 skc internet gateway 
*/
resource "aws_internet_gateway" "skc_internet_gw" {
  vpc_id = aws_vpc.skc_vpc_main.id
  tags = {
    Name   = "skc_int_gw"
    source = "terraform"
  }
}


# /*
# SubNet
# */

resource "aws_subnet" "skc_public" {
  vpc_id                  = aws_vpc.skc_vpc_main.id
  cidr_block              = "10.0.0.0/16"
  map_public_ip_on_launch = true
  availability_zone       = "us-east-2a"
  tags = {
    Name   = "Skc Public Subnet"
    source = "terraform"
  }
}


/*
Security Groups
*/

resource "aws_security_group" "skc_elb_sg" {
  name   = "skc_elb_sg"
  vpc_id = aws_vpc.skc_vpc_main.id
  #vpc_id = "vpc-d7589ebc"

  #incoming  htps web traffic
  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  #incoming  http web traffic
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  #Outgoing  traffic
  egress {
    from_port   = 0
    protocol    = -1
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]

  }
  tags = {
    Name   = "skc_elb_sg"
    source = "terraform"
  }
}

module "skc_elb" {
  source              = "./modules/skc_elb"
  enable              = var.skc_enable
  certpem             = var.certpem
  certkey             = var.certkey
  subnets             = [aws_subnet.skc_public.id]
  security_groups     = [aws_security_group.skc_elb_sg.id]
  elb_security_policy = "ELBSecurityPolicy-TLS-1-2-2017-01"
  cert_applied        = var.cert_to_use
} //end skc_elb
