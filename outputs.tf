


output "skc_vpc" {
  value = aws_vpc.skc_vpc_main.id
}

output "main_skc_elb_sg" {
  value = aws_security_group.skc_elb_sg.id
}

output "main_skc_subnet_id" {
  value = aws_subnet.skc_public.id
}

output "var_skc_enable" {
  value = var.skc_enable
}

output "module_mod_id_arn" {
  value = module.skc_elb.mod_id_arn
}
